#1 POS tagging accuracy

94.5%


#2 Precision Recall and FSCORE of NE  for NER

Class|Precision|Recall|F-Score
--|--|--|--
MISC|0.325|0.242|0.277
LOC|0.667|0.521|0.585
ORG|0.546|0.531|0.539
PER|0.546|0.460|0.499


Overall F-score = 0.513274

#3 Naive Bayes instead of perceptron:

With POS, the Overall F-score decreased to 0.47

Low scores are because of lack of information about each words (features). For example: In perceptron, we have information about previous, next, prefixes, suffixes, word shape etc. But in Naive bayes, we just have the knowledge of occurrences of each word in their respective classes.