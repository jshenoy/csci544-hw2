import sys
from collections import Counter
from collections import defaultdict
import pickle
import codecs

def get_argmax_tag(perceptron, words):
	maxv = float("-inf")
	maxk = str()
	for cl in perceptron:
		value = 0
		for w in words:
			value += perceptron[cl][w]
		if value > maxv:
			maxv = value
			maxk = cl

	return maxk	

debug = 0

def postagfn2(path1):
	wavg = pickle.load(open(path1,"rb"))

	fdummy = 'p:none '
	ldummy = ' n:none'

	auxin = str()  # small buffer to hold the words following the class

	# p - previous
	# c - current
	# n - next
	right = 0
	count = 0	
	for line in sys.stdin.readlines():

		taggedline = str()

		ltmp = line.split()

		#debug only
		if debug:
			gs = ltmp[0].split('/')[1]


		fpre = fdummy
		fcur = ltmp[0].split('/')[0]
		try:
			f3 = ltmp[0].split('/')[0][-3:]
			f4 = ltmp[0].split('/')[0][-2:]
		except:
			f3 = ltmp[0].split('/')[0][-1:]
			f4 = ltmp[0].split('/')[0][-1:]		
		


		if (len(ltmp) > 1):
			auxin = fdummy + ' c:' + fcur + ' n:' + ltmp[1].split('/')[0] + ' f3:' + f3 + ' f4:' + f4  + '\n'
		else:
			auxin = fdummy + ' c:' + fcur + ldummy + ' f3:' + f3 + ' f4:' + f4 + '\n'
			answer = get_argmax_tag(wavg,auxin.split())
			taggedline = fcur + '/' + answer
			print (taggedline)
			continue

		answer = get_argmax_tag(wavg,auxin.split())

		if debug:
			if (answer == gs):
				right += 1
			count += 1		

		taggedline = fcur + '/' + answer

		for pwrd, cwrd, nwrd in zip(ltmp, ltmp[1:], ltmp[2:]):

			auxin = 'p:' + pwrd.split('/')[0]
			auxin += ' c:' + cwrd.split('/')[0]
			auxin += ' n:' + nwrd.split('/')[0]
			try:
				auxin += ' f3:' + cwrd.split('/')[0][-3:]
				auxin += ' f4:' + cwrd.split('/')[0][-2:] + '\n'
			except:
				auxin += ' f3:' + cwrd.split('/')[0][-1:]
				auxin += ' f4:' + cwrd.split('/')[0][-1:] + '\n'			

			answer = get_argmax_tag(wavg,auxin.split())

			if debug:
				gs = cwrd.split('/')[1]
				if (answer == gs):
					right += 1
				count += 1

			
			taggedline += ' ' + cwrd.split('/')[0] + '/' + answer


		fpre = ltmp[len(ltmp)-2].split('/')[0]
		fcur = ltmp[len(ltmp)-1].split('/')[0]
		fnex = ldummy
		try:
			f3 = ltmp[len(ltmp)-1].split('/')[0][-3:]
			f4 = ltmp[len(ltmp)-1].split('/')[0][-2:]
		except:
			f3 = ltmp[len(ltmp)-1].split('/')[0][-1:]
			f4 = ltmp[len(ltmp)-1].split('/')[0][-1:]

		auxin =  ' p:' + fpre + ' c:' + fcur + fnex + ' f3:' + f3 + ' f4:' + f4 + '\n'
		
		answer = get_argmax_tag(wavg,auxin.split())

		if debug:
			gs = ltmp[len(ltmp)-1].split('/')[1]
			if (answer == gs):
				right += 1
			count += 1

		taggedline += ' ' + fcur + '/' + answer

		print (taggedline)
		sys.stdout.flush()

		if debug:
			print (right/count)



if __name__=='__main__':
	sys.exit( postagfn2(sys.argv[1]) )
#paths = list()
#for arg in sys.argv:
#	paths.append(arg)

#postag(paths)