import sys
from collections import Counter
from collections import defaultdict
import pickle
import codecs


sys.path.append('../')
import perceplearn


def postrain(paths):
	fin = codecs.open(paths[1],'r', 'latin_1')
	fdummy = 'p:none '
	ldummy = ' n:none'

	sin = str()
	auxin = str()  # small buffer to hold the words following the class

	# p - previous
	# c - current
	# n - next
	for line in fin:
		ltmp = line.split()

		fcur = ltmp[0].split('/')[0]
		try:
			f3 = ltmp[0].split('/')[0][-3:]
			f4 = ltmp[0].split('/')[0][-2:]
		except:
			f3 = ltmp[0].split('/')[0][-1:]
			f4 = ltmp[0].split('/')[0][-1:]


		if (len(ltmp) > 1):
			auxin = fdummy + ' c:' + fcur + ' n:' + ltmp[1].split('/')[0] + ' f3:' + f3 + ' f4:' + f4 + '\n'
			sin += ltmp[0].split('/')[1] + ' ' + auxin
		else:
			auxin = fdummy + ' c:' + fcur + ldummy + ' f3:' + f3 + ' f4:' + f4 + '\n'
			sin += ltmp[0].split('/')[1] + ' ' + auxin
			continue

		smartcount = 0 # counter to count groups of 3
		wclass = str() # the class

		for pwrd, cwrd, nwrd in zip(ltmp, ltmp[1:], ltmp[2:]):

			auxin = 'p:' + pwrd.split('/')[0]
			auxin += ' c:' + cwrd.split('/')[0]
			auxin += ' n:' + nwrd.split('/')[0]
			try:
				auxin += ' f3:' + cwrd.split('/')[0][-3:]
				auxin += ' f4:' + cwrd.split('/')[0][-2:] + '\n'
			except:
				auxin += ' f3:' + cwrd.split('/')[0][-1:]
				auxin += ' f4:' + cwrd.split('/')[0][-1:] + '\n'

			wclass = cwrd.split('/')[1]

			sin += wclass + ' ' + auxin

		fpre = ltmp[len(ltmp)-2].split('/')[0]
		fcur = ltmp[len(ltmp)-1].split('/')[0]
		try:
			f3 = ltmp[len(ltmp)-1].split('/')[0][-3:]
			f4 = ltmp[len(ltmp)-1].split('/')[0][-2:]
		except:
			f3 = ltmp[len(ltmp)-1].split('/')[0][-1:]
			f4 = ltmp[len(ltmp)-1].split('/')[0][-1:]		

		auxin =  ' p:' + fpre + ' c:' + fcur + ldummy + ' f3:' + f3 + ' f4:' + f4 + '\n'
		sin += ltmp[len(ltmp)-1].split('/')[1] + auxin

	#print (sin)
	finterw = codecs.open('../pos_inter','w','latin_1')
	finterw.write(sin)
	#perceplearn.perceplearnfn('pos_inter',paths[2],'pos.dev')
	perceplearn.perceplearnfn('../pos_inter',paths[2])




	fin.close()
	finterw.close()


paths = list()
for arg in sys.argv:
	paths.append(arg)

postrain(paths)