import sys
from collections import Counter
from collections import defaultdict
import pickle
import codecs
import argparse
#sys.path.append('./ner/')
#import netag

#temporary
try:
	import postag_dev
	import nertag
except ImportError:
	pass
#learns and generates pos.nb

def get_argmax(perceptron, words):
	maxv = float("-inf")
	maxk = str()
	for cl in perceptron:
		value = 0
		for w in words[1:]:
			value += perceptron[cl][w]
		if value > maxv:
			maxv = value
			maxk = cl

	return maxk	


def perceplearnfn(tf, mf, df=None):

	fin = codecs.open(tf,'r','latin_1')
	perceptron = defaultdict(Counter)
	wavg = defaultdict(Counter)

	#Initialize all to 0
	for line in fin:

		words = line.split()
		cl = words[0]

		for wrd in words[1:]:
			perceptron[cl][wrd] = 0
			wavg[cl][wrd] = 0

	# Is fx = 1?
	fx = 1

	fin.seek(0)

	for i in range(20):
		for line2 in fin:

			words = line2.split()
			curcl = words[0]
			z = get_argmax(perceptron, words)
			if (z != curcl):
				for x in words[1:]:
					perceptron[z][x] -= fx
					perceptron[curcl][x] += fx
					if (wavg[z][x] == 0):
						wavg[z][x] = 0
					if(wavg[curcl][x] == 0):
						wavg[curcl][x] = 0

		# Reset the file pointer so that you can do another iter
		fin.seek(0)

		#Weighted average for each line
		for key1 in perceptron:
			wavg[key1].update(perceptron[key1])


		if df is not None:
			print ("iter:" + str(i))
			#pickle.dump(wavg, open(mf,"wb"))
			#netag.postagfn('ner.m')

	pickle.dump(wavg, open(mf,"wb"))
	#print (len(perceptron))
	#print (wavg)



if __name__ == "__main__":
# Neat way to parse arguments
	parser = argparse.ArgumentParser(description='Perceplearn')
	parser.add_argument('tfile', help='Path to training file')
	parser.add_argument('mfile', help='Path to Model file')
	parser.add_argument('-d', action='store', dest='devfile', help='Path to Dev File')	
	pa = parser.parse_args()

	perceplearnfn(pa.tfile, pa.mfile, pa.devfile)
