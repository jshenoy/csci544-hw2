import sys
from collections import Counter
from collections import defaultdict
import pickle
import codecs
import fileinput

def get_argmax_tag(perceptron, words):
	maxv = float("-inf")
	maxk = str()
	for cl in perceptron:
		value = 0
		for w in words:
			value += perceptron[cl][w]
		if value > maxv:
			maxv = value
			maxk = cl

	return maxk	

def percepclassifyfn(paths):
	wavg = pickle.load(open(paths[1],"rb"))
	sys.stdin = codecs.open(sys.stdin.fileno(), encoding='latin_1', mode='r')
	for line in sys.stdin.readlines():
		maxclass = get_argmax_tag(wavg,line.split())
		print (maxclass)
	return maxclass



# if __name__=='__main__':
# 	sys.exit( percepclassify(sys.argv[1],sys.argv[2]) )
paths = list()
for arg in sys.argv:
	paths.append(arg)

percepclassifyfn(paths)