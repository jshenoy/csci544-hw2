import sys
from collections import Counter
from collections import defaultdict
import pickle
import codecs
import re

sys.path.append('../')
import perceplearn

def wordshape(feature):
	small = re.sub('[a-z]+','a',feature)
	small = re.sub('[A-Z]+','A',small)
	small = re.sub('[0-9]+','9',small)
	small = re.sub('[^\w]+','&',small)

	return small

def postrain(paths):
	fin = codecs.open(paths[1],'r', 'latin_1')
	fdummy = 'p:none/-BEG- '
	ldummy = ' n:none/-END-'

	ftagdummy = 'FTAG'

	sin = str()
	auxin = str()  # small buffer to hold the words following the class
	#previous_tag = str();
	wshape = str()
	ppos = str()
	npos = str()


	# p - previous
	# c - current
	# n - next
	for line in fin:
		ltmp = line.split()

		curwrd = ltmp[0].rsplit('/',2)[0] + '/' + ltmp[0].rsplit('/',2)[1]
		wshape = wordshape(ltmp[0].rsplit('/',2)[0])
		ppos = '-B'
		npos = '-E'


		if (len(ltmp) > 1):
			nxtwrd = ltmp[1].rsplit('/',2)[0] + '/' + ltmp[1].rsplit('/',2)[1]
			npos = ltmp[1].rsplit('/',2)[1]
			auxin = fdummy + ' c:' + curwrd + ' n:' + nxtwrd
			sin += ltmp[0].rsplit('/',2)[2] + ' ' + auxin + ' ws:' + wshape + ' pp:' + ppos + ' np:' + npos +  '\n'#' pt:' + ftagdummy + '\n'
		else:
			auxin = fdummy + ' c:' + curwrd + ldummy
			sin += ltmp[0].rsplit('/',2)[2] + ' ' + auxin + ' ws:' + wshape + ' pp:' + ppos + ' np:' + npos + '\n' #' pt:' + ftagdummy + '\n'
			continue

		#previous_tag = ltmp[0].rsplit('/',2)[2];
		smartcount = 0 # counter to count groups of 3
		wclass = str() # the class

		for pwrd, cwrd, nwrd in zip(ltmp, ltmp[1:], ltmp[2:]):

			auxin = 'p:' + pwrd.rsplit('/',2)[0] + '/' + pwrd.rsplit('/',2)[1]
			auxin += ' c:' + cwrd.rsplit('/',2)[0] + '/' + cwrd.rsplit('/',2)[1]
			auxin += ' n:' + nwrd.rsplit('/',2)[0] + '/' + nwrd.rsplit('/',2)[1]
			ppos = pwrd.rsplit('/',2)[1]
			npos = nwrd.rsplit('/',2)[1]

			wclass = cwrd.rsplit('/',2)[2]
			wshape = wordshape(cwrd.rsplit('/',2)[0])

			sin += wclass + ' ' + auxin + ' ws:' + wshape + ' pp:' + ppos + ' np:' + npos + '\n' #' pt:' + previous_tag + '\n'
			#previous_tag = wclass

		prewrd = ltmp[len(ltmp)-2].rsplit('/',2)[0] + '/' + ltmp[len(ltmp)-2].rsplit('/',2)[1]
		curwrd = ltmp[len(ltmp)-1].rsplit('/',2)[0] + '/' + ltmp[len(ltmp)-1].rsplit('/',2)[1]
		ppos = ltmp[len(ltmp)-2].rsplit('/',2)[1]
		npos = '-E'
		auxin =  ' p:' + prewrd + ' c:' + curwrd + ldummy
		wshape = wordshape(ltmp[len(ltmp)-1].rsplit('/',2)[0])
		sin += ltmp[len(ltmp)-1].rsplit('/',2)[2] + auxin + ' ws:' + wshape + ' pp:' + ppos + ' np:' + npos + '\n' #' pt:' + previous_tag + '\n'

	#print (sin)
	finterw = codecs.open('../ner_inter','w','latin_1')
	finterw.write(sin)
	#perceplearn.perceplearnfn('../ner_inter',paths[2],'ner.esp.dev')
	perceplearn.perceplearnfn('../ner_inter',paths[2])


	fin.close()
	finterw.close()


paths = list()
for arg in sys.argv:
	paths.append(arg)

postrain(paths)