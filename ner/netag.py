import sys
from collections import Counter
from collections import defaultdict
import pickle
import codecs
import re

def get_argmax_tag(perceptron, words):
	maxv = float("-inf")
	maxk = str()
	for cl in perceptron:
		value = 0
		for w in words:
			value += perceptron[cl][w]
		if value > maxv:
			maxv = value
			maxk = cl

	return maxk	


debug = 0

def wordshape(feature):
	small = re.sub('[a-z]+','a',feature)
	small = re.sub('[A-Z]+','A',small)
	small = re.sub('[0-9]+','9',small)
	small = re.sub('[^\w]+','&',small)

	return small


def postagfn(path1):
	wavg = pickle.load(open(path1,"rb"))

	fdummy = 'p:none '
	ldummy = ' n:none'

	auxin = str()  # small buffer to hold the words following the class
	wshape = str()
	ppos = str()
	npos = str()

	# p - previous
	# c - current
	# n - next
	right = 0
	count = 0	
	sys.stdin = codecs.open(sys.stdin.fileno(), encoding='latin_1', mode='r')	
	#devfile = codecs.open('ner.esp.dev',encoding='latin_1' , mode='r')
	for line in sys.stdin.readlines():

		taggedline = str()

		ltmp = line.split()

		fpre = fdummy
		fcur = ltmp[0].rsplit('/',2)[0] + '/' + ltmp[0].rsplit('/',2)[1]
		#previous_tag = 'FTAG'
		wshape = wordshape(ltmp[0].rsplit('/',2)[0])
		ppos = '-B'
		npos = '-E'


		if (len(ltmp) > 1):
			fnex = ltmp[1].rsplit('/',2)[0] + '/' + ltmp[1].rsplit('/',2)[1]
			npos = ltmp[1].rsplit('/',2)[1]
			auxin = fdummy + ' c:' + fcur + ' n:' + fnex + ' ws:' + wshape + ' pp:' + ppos + ' np:' + npos + '\n'# ' pt:' + previous_tag + '\n'
		else:
			auxin = fdummy + ' c:' + fcur + ldummy + ' ws:' + wshape + ' pp:' + ppos + ' np:' + npos + '\n'#' pt:' + previous_tag + '\n'
			answer = get_argmax_tag(wavg,auxin.split())
			taggedline = fcur + '/' + answer
			print (taggedline)
			continue

		answer = get_argmax_tag(wavg,auxin.split())
		#previous_tag = answer
		#print (auxin)

		if debug:
			gs = ltmp[0].rsplit('/',2)[2]
			if (answer == gs):
				right += 1
			count += 1				

		taggedline = fcur + '/' + answer

		for pwrd, cwrd, nwrd in zip(ltmp, ltmp[1:], ltmp[2:]):

			wshape = wordshape(cwrd.rsplit('/',2)[0])
			ppos = pwrd.rsplit('/',2)[1]
			npos = nwrd.rsplit('/',2)[1]
			auxin = 'p:' + pwrd.rsplit('/',2)[0] + '/' + pwrd.rsplit('/',2)[1]
			auxin += ' c:' + cwrd.rsplit('/',2)[0] + '/' + cwrd.rsplit('/',2)[1] 
			auxin += ' n:' + nwrd.rsplit('/',2)[0] + '/' + nwrd.rsplit('/',2)[1]
			auxin += ' ws:' + wshape + ' pp:' + ppos + ' np:' + npos + '\n'
			#auxin +=  ' pt:' + previous_tag+ '\n'

			answer = get_argmax_tag(wavg,auxin.split())

			cw = cwrd.rsplit('/',2)[0]
			cn = cwrd.rsplit('/',2)[1]

			if debug:
				gs = cwrd.rsplit('/',2)[2]
				if (answer == gs):
					right += 1
				count += 1			
			
			taggedline += ' ' + cw + '/' + cn + '/' + answer
			#previous_tag = answer
			#print (auxin)


		wshape = wordshape(ltmp[len(ltmp)-1].rsplit('/',2)[0])
		ppos = ltmp[len(ltmp)-2].rsplit('/',2)[1]
		npos = '-E'
		fpre = ltmp[len(ltmp)-2].rsplit('/',2)[0] + '/' + ltmp[len(ltmp)-2].rsplit('/',2)[1]
		fcur = ltmp[len(ltmp)-1].rsplit('/',2)[0] + '/' + ltmp[len(ltmp)-1].rsplit('/',2)[1]
		fnex = ldummy
		auxin =  'p:' + fpre + ' c:' + fcur + fnex + ' ws:' + wshape + ' pp:' + ppos + ' np:' + npos + '\n'#' pt:' + previous_tag + '\n'
		
		answer = get_argmax_tag(wavg,auxin.split())
		#print (auxin)		

		if debug:
			gs = ltmp[len(ltmp)-1].rsplit('/',2)[2]
			if (answer == gs):
				right += 1
			count += 1		

		taggedline += ' ' + fcur + '/' + answer

		print (taggedline)
		sys.stdout.flush()


	if debug:
		print (right/count)		



if __name__=='__main__':
	sys.exit( postagfn(sys.argv[1]) )
#paths = list()
#for arg in sys.argv:
#	paths.append(arg)

#postag(paths)